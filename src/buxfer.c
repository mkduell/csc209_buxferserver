#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lists.h"
#include "buxfer.h"
#include "wrapsock.h"
#include "util.h"

#define INPUT_ARG_MAX_NUM 5
#define DELIM " \n\r"

int process_args(int cmd_argc, char **cmd_argv, Group **group_list_addr,
					Client* c,Client* client_list) 
{
    Group *group_list = *group_list_addr; 
    Group *g;


	char* gdne="Group %s does not exist\r\n";
	int len_gdne=24;
	char* msg = NULL;
	
    if (cmd_argc <= 0) 
    {
        return 0;
    } 
    else if (strcmp(cmd_argv[0], "quit") == 0 && cmd_argc == 1) 
    {

		Write(c->soc,"Goodbye\r\n",strlen("Goodbye\r\n"));
		return -1;
        
   	} 
   	else if (strcmp(cmd_argv[0], "add_group") == 0 && cmd_argc == 2) 
   	{

		//add new group
		msg = add_group(group_list_addr,cmd_argv[1]);
		Write(c->soc,msg,strlen(msg));
		free(msg);
		
		//add current user to group
		g = find_group(*group_list_addr,cmd_argv[1]);
		msg = add_user(g,c->name);
		Write(c->soc,msg,strlen(msg));
		
		
		//Send message to others in group that a new users has been added
		User* users = NULL;
		for(users = g->users;users!=NULL;users=users->next)
		{
			if(strcmp(c->name,users->name)!=0)
			{
				int i=0;
				for(i=0;i<MAXCLIENTS;i++)
				{
					if(client_list[i].name!=NULL && 
							strcmp(client_list[i].name,users->name)==0)
					{
						Write(client_list[i].soc,msg,strlen(msg));
					}
				}
			}
		}
		free(msg);
        
    } 
    else if (strcmp(cmd_argv[0], "list_groups") == 0 && cmd_argc == 1) 
    {
        msg=list_groups(group_list);
		Write(c->soc,msg,strlen(msg));
		free(msg);
    }
	else if (strcmp(cmd_argv[0], "list_users") == 0 && cmd_argc == 2) 
	{
        if ((g = find_group(group_list, cmd_argv[1])) == NULL) 
        {
			//Message for group not existing
			msg=myMalloc(sizeof(char)*(strlen(cmd_argv[1])+len_gdne));
        	snprintf(msg,strlen(cmd_argv[1])+len_gdne,gdne,cmd_argv[1]);
			Write(c->soc,msg,strlen(msg));
			free(msg);
        } 
        else 
        {
            msg=list_users(g);
			Write(c->soc,msg,strlen(msg));
			free(msg);
		}        
	} 
	else if (strcmp(cmd_argv[0], "user_balance") == 0 && cmd_argc == 2) 
	{
		if ((g = find_group(group_list, cmd_argv[1])) == NULL) 
		{
			//Message for group not existing
			msg=myMalloc(sizeof(char)*(strlen(cmd_argv[1])+len_gdne));
			snprintf(msg,strlen(cmd_argv[1])+len_gdne,gdne,cmd_argv[1]);
			Write(c->soc,msg,strlen(msg));
			free(msg);
		} 
		else	//group found
		{
			msg=user_balance(g, c->name); 
			Write(c->soc,msg,strlen(msg));
			free(msg);
		}        
	} 
	else if (strcmp(cmd_argv[0], "add_xct") == 0 && cmd_argc == 3) 
	{
        if ((g = find_group(group_list, cmd_argv[1])) == NULL) 
        {
			//Message for group not existing
			msg=myMalloc(sizeof(char)*(strlen(cmd_argv[1])+len_gdne));
        	snprintf(msg,strlen(cmd_argv[1])+len_gdne,gdne,cmd_argv[1]);
        	Write(c->soc,msg,strlen(msg));
        	free(msg);
        } 
        else 
        {
            char *end;
            double amount = strtod(cmd_argv[2], &end);
            if (end == cmd_argv[2]) {
				Write(c->soc,"Incorrect number format\r\n",strlen("Incorrect number format\r\n"));
            } 
            
            
            

            /*
                MAX_DOUBLE_LEN includes space for a .
            	This check is to make sure that a string value too large for
            	a double is not accepted. If the number is too big the number 
            	will overflow and store an incorrect value.
            	
            	on CDF a double is currently 8 bytes => max (unsigned) value
            	of a double being 4294967295. signed max value being 2147483647.
            	
            	This is 10 digits, for simplicity will only accept up to 9 
            	digits left of the decimal point to ensure the double being
            	converted is valid.

            	gt 9 with no decimal 
            	|| gt 12 (9 left of dec + dec + 2 after dec)
	            || gt 11 (9 left of dec + dec + 1 after dec)
	            || gt 10 (9 left of dec + dec + 0 after dec)	            
            */
	       if( ((strlen(cmd_argv[2]) > 9 )&& !strchr(cmd_argv[2],'.')) 
            	|| ((strlen(cmd_argv[2]) > (9+3)) && strchr(cmd_argv[2],'.') && 
            			strlen(strchr(cmd_argv[2],'.')+1)==2)
            	|| ((strlen(cmd_argv[2]) > (9 +2)) && strchr(cmd_argv[2],'.') && 
            			strlen(strchr(cmd_argv[2],'.')+1)==1)
            	|| ((strlen(cmd_argv[2]) > (9 +1)) && strchr(cmd_argv[2],'.') && 
            			strlen(strchr(cmd_argv[2],'.')+1)==0)  
            	
            	)
            { 
            	//invalid double, it is too big.
            	Write(c->soc,"Incorrect number format\r\n",strlen("Incorrect number format\r\n"));
            }
            else //Add the transaction now. 
            {
				msg=add_xct(g, c->name, amount);
				Write(c->soc,msg,strlen(msg));
				free(msg);
			}
        }
    } 
    else 
    {
		Write(c->soc,"Incorrect syntax\r\n",strlen("Incorrect syntax\r\n"));
    }
    return 0;
}

int runBuxferCommand(char* input,Client* c,Group** group_list,Client* client_list)
{
    char *cmd_argv[INPUT_ARG_MAX_NUM];
    int cmd_argc;

	/* Tokenize arguments */
    char *next_token = strtok(input, DELIM);
    cmd_argc = 0;
    while (next_token != NULL) 
	{
		if (cmd_argc >= INPUT_ARG_MAX_NUM - 1) 
		{
			Write(c->soc,"Too many arguments!\r\n",strlen("Too many arguments!\r\n"));
			cmd_argc = 0;
			break;
		}
		cmd_argv[cmd_argc] = next_token;
		cmd_argc++;
        next_token = strtok(NULL, DELIM);
	}
	cmd_argv[cmd_argc] = NULL;
	if (cmd_argc > 0 && 
			process_args(cmd_argc, cmd_argv, group_list,c,client_list) == -1) 
	{
		return -1;
	}
	else
	{
		return 0;
	}
}



void releaseClient(Client* c)
{
	Close(c->soc);	
	c->soc=-1;
	bzero(c->buf,MAXLINE); //clear out for reuse
	c->curpos=0;
	free(c->name); //free memory
	c->name=NULL; //mark as NULL so future ones can be processed correctly
}

