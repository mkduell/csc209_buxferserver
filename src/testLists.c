#include <stdio.h>

#include "lists.h"
#include "util.h"


int test_add_group_1()
{
	Group* head=NULL;
	
	printf("Test:Expecting to be able to add group...\n");	
	char* msg = add_group(&head,"Group One");
	printf("%s\n",msg);
	if(hasKey(msg,ADD_GROUP_KEY)!=0)
	{
		printf("Fail:Could not add group, but was supposed to be able to\n");
		return -1;
	}
	printf("Success...\n");

	/*now test for double adding, should be an error */

	printf("Test:Expecting that Group one should not be added again...\n");
	msg = add_group(&head,"Group One");
	printf("%s\n",msg);
	if(hasKey(msg,ADD_GROUP_FAIL_KEY)!=0)
	{
		printf("Fail:Was expecting to NOT be able to add group again, but was able to\n");
		return -1;
	}
	printf("Success...\n");

	printf("Test:Expecting to be able to add to non-empty group...\n");
	msg = add_group(&head,"Group Two");
	printf("%s\n",msg);
	if(hasKey(msg,ADD_GROUP_KEY)!=0)
	{
		printf("Fail:Could not add group, but was supposed to be able to\n");
		return -1;
	}
	printf("Success...\n");
	
	return 0;
}
int test_add_user1()
{
	Group* head=NULL;
	
	add_group(&head,"Group One");
	
	printf("Test:Expecting to be able to add a user...\n");
	char* msg = add_user(head,"Sammy");
	printf("%s\n",msg);
	if(hasKey(msg,ADD_USER_SUCCESS_KEY)!=0)
	{
		printf("Fail:Could not add user, but was supposed to be able to\n");
		return -1;
	}
	printf("Success...\n");

	/*test adding in a double user */
	printf("Test:Expecting that a second user by the same name cannot be added...\n");
	msg = add_user(head,"Sammy");
	printf("%s\n",msg);
	if(hasKey(msg,ADD_USER_FAIL_KEY)!=0)
	{
		printf("Fail:Added user,but was not supposed to be able to\n");
		return -1;
	}
	printf("Success...\n");

	/*test adding a user to a non-empty group */
	printf("Test:Expecting to be able to add a user to a non-empty list...\n");
	msg = add_user(head,"Celise");
	printf("%s\n",msg);
	if(hasKey(msg,ADD_USER_SUCCESS_KEY)!=0)
	{
		printf("Fail:Could not add user, but was supposed to be able to\n");
		return -1;
	}
	printf("Success...\n");
	return 0;
}
int test_list_group()
{
	Group* head=NULL;
	char newLine='\n';

	printf("Test:0 groups should be printed...\n");
	char* msg = list_groups(head);
	printf("%s\n",msg);
	int count = numChrOccours(msg,newLine);
	
	if(count!=0)
	{
		printf("Fail:Was expecting %d lines, only got %d\n",0,count);
		return -1;
	}
	printf("Success...\n");

	add_group(&head,"Group One");
	add_group(&head,"Group Two");
	add_group(&head,"Group Three");
	add_group(&head,"Group Four");
	add_group(&head,"Group Five");

	printf("Test:Five Groups should be printed...\n");
	msg = list_groups(head);
	printf("%s\n",msg);

	count=numChrOccours(msg,newLine);
	if(count!=5)
	{
		printf("Fail:Was expecting %d lines, only got %d\n",5,count);
		return -1;
	}
	printf("Success...\n");
	
	
	return 0;
}
int test_list_users()
{
	Group* head = NULL;
	char newLine='\n';
	
	printf("Test: 0 users should be printed...\n");
	add_group(&head,"Group One");
	char* msg=list_users(head);
	printf("%s\n",msg);
	int count=numChrOccours(msg,newLine);

	if(count!=0)
	{
		printf("Fail:Was expecting %d lines, got %d\n",0,count);
		return -1;
	}
	printf("Success...\n");

	printf("Test:4 users should be printed...\n");
	add_user(head,"Tammy");
	add_user(head,"Sammy");
	add_user(head,"Celise");
	add_user(head,"Maegen");

	msg=list_users(head);
	printf("%s\n",msg);
	count=numChrOccours(msg,newLine);
	if(count!=4)
	{
		printf("Fail:Was expecting %d lines, got %d\n",4,count);
		return -1;
	}
	printf("Success...\n");


	return 0;
}
int test_user_balance()
{

	Group* head = NULL;
	add_group(&head,"Group One");
	add_user(head,"Sammy");
	
	printf("Test:Expecting not be able to get balance of non-existant user...\n");
	char* msg = user_balance(head,"Tammy");
	printf("msg:\n%s\n",msg);
	if(hasKey(msg,USER_BALANCE_FAIL_KEY)!=0)
	{
		printf("Fail:Should not have been able toget balance of non-existant user\n");
		return -1;
	}
	printf("Success...\n");

	add_user(head,"Tammy");
	printf("Test:Expecting to be able to get balance of existant user...\n");
	msg = user_balance(head,"Tammy");
	printf("msg:\n%s\n",msg);
	if(hasKey(msg,USER_BALANCE_KEY)!=0)
	{
		printf("Fail:Should have been able to get balance of existant user\n");
		return -1;
	}
	printf("Success...\n");

	return 0;
}
int test_add_xct()
{
	Group* head = NULL;
	add_group(&head,"Group One");
	
	printf("Test:Expecting not to be able to get balance of non-existant user...\n");
	char* msg = add_xct(head,"Sammy",12.34);
	printf("msg:\n%s\n",msg);
	if(hasKey(msg,ADD_XCT_FAIL_KEY)!=0)
	{
		printf("Fail:Should not have been able to get balance of non-existant user\n");
		return -1;
	}
	printf("Success...\n");
	
	add_user(head,"Sammy");
	printf("Test:Expecting to be able to get balance of existant user...\n");
	msg = add_xct(head,"Sammy",12.34);
	printf("msg:\n%s\n",msg);
	if(hasKey(msg,ADD_XCT_SUCCESS_KEY)!=0)
	{
		printf("Fail:Should have been able to get balance of existant user\n");
		return -1;
	}
	printf("Success...\n");

	
	return 0;
}

int main(int argc, char** argv)
{
	if(test_add_group_1()!=0)
	{
		printf("Error, test_add_group_1 did not pass. Check test output\n");
		return -1;
	}
	if(test_add_user1()!=0)
	{
		printf("Error, test_add_user1 did not pass. Check test output\n");
		return -1;
	}
	if(test_list_group()!=0)
	{
		printf("Error, test_list_group did not pass. Check test output\n");
		return -1;
	}
	if(test_list_users()!=0)
	{
		printf("Error, test_list_users did not pass. Check test output\n");
		return -1;
	}
	if(test_user_balance()!=0)
	{
		printf("Error, test_user_balance did not pass. Check test output\n");
		return -1;
	}
	if(test_add_xct()!=0)
	{
		printf("Error, test_add_xct did not pass. Check test output\n");
		return -1;
	}
	return 0;
}
