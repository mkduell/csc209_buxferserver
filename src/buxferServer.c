/***** inetserver.c *****/ 
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <stdlib.h>        /* for getenv */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>    /* Internet domain header */
#include <time.h>
#include "wrapsock.h"		//Socket wrappers
#include "lists.h"			//buxfer structs
#include "util.h" 			//myMalloc
#include "buxfer.h" 		//interface to buxfer, struct def

#ifndef PORT
#define PORT 30000
#endif

int isClientReadyForCommands(const Client const *c)
{
	return (c->name==NULL)? -1:0;
}
/*  read from the client
 * 	returns -1 if the socket needs to be closed and 0 otherwise */
int readfromclient(Client *c,Group** group_list,Client* client_list) 
{
	char *startptr = &c->buf[c->curpos];
	int len = read(c->soc, startptr, MAXLINE - c->curpos);
	if(len <= 0) {
		if(len == -1) {
			perror("read on socket");
		}
		return -1;
	} 
	else 
	{
		/* checks if we have filled up the buffer from read and do NOT have
			a full line i.e. it is 'incomplete'. Then the input is too long
			for the server to process 
			*/
		if(len >= MAXLINE && !(strchr(c->buf,'\n') || strchr(c->buf,'\n')))
		{
			char* lineTooLongMsg = "Command entered is too long\r\n";
			//write error, clear buf and reset curpos to the beginning.
			c->curpos=0;
			//c->buf[0]='\0';
			Write(c->soc,lineTooLongMsg,strlen(lineTooLongMsg)+1);	
			return 0;
		}
		c->curpos += len;
		c->buf[c->curpos] = '\0';

		/* Check if there is a full command/line to process*/
		if (strchr(c->buf, '\n') || strchr(c->buf, '\r')) 
		{		
			//cut out the line(s) to execute, and execute them
			while(strchr(c->buf, '\n') || strchr(c->buf, '\r')) 
			{
				//printf("Got '%s' from client.\n", c->buf);
			
				/* first extract the command  */
				char *leftover = NULL;												
				char* nl_ptr = strchr(c->buf,'\n');
				char* cr_ptr = strchr(c->buf,'\r');
			
				//check \r pos
				//check \n pos
				if(nl_ptr!=NULL && cr_ptr !=NULL && cr_ptr+1==nl_ptr) //implies \r\n
				{			
					//printf("received \\r\\n\n");
					*nl_ptr='\0';
					*cr_ptr='\0';			
					leftover = nl_ptr+1;
				}
				else
				{
					if(nl_ptr!=NULL) // implies \n
					{
						//printf("received \\n\n");
						*nl_ptr='\0';					
						leftover = nl_ptr+1;
					}
					if(cr_ptr!=NULL) //implies \r
					{
						//printf("received \\r\n");
						*cr_ptr='\0';
						leftover = cr_ptr+1;
					}
				}
				//printf("leftover = %s\n",leftover+1);			
				//printf("conferting first \\n and \\r to nullbyte yields '%s'\n", c->buf);
			
				//now that the command segment is extracted, process.			
				if(isClientReadyForCommands(c)==0)
				{
					if(runBuxferCommand(c->buf,c,group_list,client_list)!=0)
					{
						return -1;
					}
				}
				else //if the client isn't ready for commands that implies
					//we didn't set up their name yet.
				{			
					c->name = myMalloc(sizeof(char)*MAXLINE);
					strncpy(c->name,c->buf,MAXLINE);
								
					char* greetingFormat = "Welcome, %s! Please enter Buxfer commands\r\n";
					int greetingLen = 42;
					char* buxferGreeting = myMalloc(sizeof(char)*(greetingLen+strlen(c->name)));
					snprintf(buxferGreeting,greetingLen+strlen(c->name),greetingFormat,c->name);
					Write(c->soc,buxferGreeting,greetingLen+strlen(c->name));			
				}

				//After the commands have been executed, shift down the
				// data.
				int len_leftover = strlen(leftover);
				memmove(c->buf, leftover, len_leftover);
				c->buf[len_leftover]='\0';
				c->curpos = strlen(c->buf);
			
			}
			return 0;
		} 
		else 
		{
			/*Don't do anything. Wait for more input. */
			return 0;
		}
	}
}


int main()
{ 
	Client client[MAXCLIENTS];
	fd_set  rset, allset;
	
    int soc, ns;
    int on = 1, status;
    int i,maxfd;
    
    struct sockaddr_in peer;
    struct sockaddr_in self; 
    unsigned int peer_len = sizeof(peer);
    
    self.sin_family = PF_INET;
    self.sin_port = htons(PORT);  

    self.sin_addr.s_addr = INADDR_ANY;
    bzero(&(self.sin_zero), 8);

    peer.sin_family = PF_INET;
    /* set up listening socket soc */
    soc = Socket(PF_INET, SOCK_STREAM, 0);
    if (soc < 0) {  
        perror("server:socket"); 
        exit(1);
    }

    // Make sure we can reuse the port immediately after the
    // server terminates.
    status = setsockopt(soc, SOL_SOCKET, SO_REUSEADDR,
                        (const char *) &on, sizeof(on));
    if(status == -1) {
        perror("setsockopt -- REUSEADDR");
    }

    // Associate the proess with the address and a port
    Bind(soc, (struct sockaddr *)&self, sizeof(self));

    // Sets up a queue in the kernel to hold pending connections
    Listen(soc, 1);                              
    printf("Listening on PORT= %d\n", PORT);
    
	maxfd = soc;   /* initialize */
	for (i = 0; i < MAXCLIENTS; i++) {
		client[i].soc = -1; /* -1 indicates available entry */
		client[i].curpos = 0;
		client[i].name=NULL;
	}

	//initialize allset
	FD_ZERO(&allset);
	FD_SET(soc, &allset);

	Group *group_list = NULL;
	for(;;)
	{
		rset = allset;      //set temp rset back to initial value
		Select(maxfd+1, &rset, NULL, NULL, NULL);
		if (FD_ISSET(soc, &rset)) /* new client connection */
		{    

		   /* accept connection request */
		    printf("Calling accept. There is a client on the socket\n");		   
			ns = Accept(soc, (struct sockaddr *) &peer, &peer_len);
			printf("Accepted a new client\n");

			for (i = 0; i < MAXCLIENTS; i++)
			{
				if (client[i].soc < 0) {
					client[i].soc = ns; /* save descriptor */
					break;
				}
			}
			printf("new client will be i=%d\n",i);
			if (i == MAXCLIENTS) 
			{
				printf("too many clients");
			}
			
			FD_SET(ns, &allset);    /* add new descriptor to set */
			if (ns > maxfd)
			{
				maxfd = ns; /* for select */
			}
			
			/*Send Greeting to new client */
			//printf("Going to say Welcome\n");

			Write(ns,"Welcome!\r\n",strlen("Welcome!\r\n"));
			char* greeting = "What is your name?\r\n";
			Write(ns,greeting,strlen(greeting));

		}		
		for (i = 0; i < MAXCLIENTS; i++) {   /* check which clients have data */
			if ( client[i].soc < 0)
				continue;
			if (FD_ISSET(client[i].soc, &rset)) 
			{
				//client has information to read. process it
				int result = readfromclient(&client[i],&group_list,client);
				
				if(result == -1)  {
					FD_CLR(client[i].soc, &allset);
					releaseClient(client+i);				
				}
			}
		}
		
	}

    return(0);
}
