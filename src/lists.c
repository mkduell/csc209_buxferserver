#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lists.h"
#include "util.h"


/* Add a group with name group_name to the group_list referred to by 
* group_list_ptr. The groups are ordered by the time that the group was 
* added to the list with new groups added to the end of the list.
*
* Returns 0 on success and -1 if a group with this name already exists.
*
* (I.e, allocate and initialize a Group struct, and insert it
* into the group_list. Note that the head of the group list might change
* which is why the first argument is a double pointer.) 
*/
char* add_group(Group **group_list_ptr, const char *group_name) {

	int nameLen = strlen(group_name);
	
	char* successMsg = myMalloc(sizeof(char)* (ADD_GROUP_SUCCESS_LEN + nameLen));
	snprintf(successMsg,ADD_GROUP_SUCCESS_LEN + nameLen ,ADD_GROUP_SUCCESS,group_name);
	
  
	 if (find_group(*group_list_ptr, group_name) == NULL) {
        //malloc space for new group
        Group *newgrp;
        if ((newgrp = myMalloc(sizeof(Group))) == NULL) {
           perror("Error allocating space for new Group");
           exit(1);
        }
        // set the fields of the new group node
        // first allocate space for the name
        int needed_space = strlen(group_name) + 1;
        if (( newgrp->name = myMalloc(needed_space)) == NULL) {
           perror("Error allocating space for new Group name");
           exit(1);
        }
        strncpy(newgrp->name, group_name, needed_space);
        newgrp->users = NULL;
        newgrp->xcts = NULL;
        newgrp->next = NULL;


        // Need to insert into the end of the list not the front
        if (*group_list_ptr == NULL) {
          // set new head to this new group -- the list was previously empty
          *group_list_ptr = newgrp;
          return successMsg;
          //return 0;
        } 
		else 
		{
        // walk along the list until we find the currently last group 
          Group * current = *group_list_ptr;
          while (current->next != NULL) {
            current = current->next;
          }
          // now current should be the last group 
          current->next = newgrp;
          return successMsg;
          //return 0;
        }
    } 
	else 
	{
		char* failMsg = myMalloc(sizeof(char)* (ADD_GROUP_FAIL_LEN + nameLen));
		snprintf(failMsg,ADD_GROUP_FAIL_LEN+ nameLen ,ADD_GROUP_FAIL,group_name);
		free(successMsg);
	
		return failMsg;
    }
}

/* Print to standard output the names of all groups in group_list, one name
*  per line. Output is in the same order as group_list.
*/
char* list_groups(Group *group_list) {
    Group * current = group_list;

	int count=0;
    int maxNameLen=0;
	while (current != NULL) 
	{
		count++;
		if(strlen(current->name)>maxNameLen)
			maxNameLen=strlen(current->name);
        current = current->next;
    }

	int lineLen = maxNameLen+LIST_GROUP_LEN;
	int groupsLen = (count*lineLen)+1;
	char* groups = myMalloc(sizeof(char)*groupsLen);
	bzero(groups,groupsLen);
	
	current=group_list;
    while (current != NULL) 
	{
		char* line = myMalloc(sizeof(char)*lineLen);	
        snprintf(line,lineLen,LIST_GROUP_MSG,current->name);

		strncat(groups,line,groupsLen);
		free(line);
        current = current->next;
    }
	return groups;
}

/* Search the list of groups for a group with matching group_name
* If group_name is not found, return NULL, otherwise return a pointer to the 
* matching group list node.
*/
Group *find_group(Group *group_list, const char *group_name) {
    Group *current = group_list;
    while (current != NULL && (strcmp(current->name, group_name) != 0)) {
        current = current->next;
    }
    return current;
}

/* Add a new user with the specified user name to the specified group. Return zero
* on success and -1 if the group already has a user with that name.
* (allocate and initialize a User data structure and insert it into the
* appropriate group list)
*/
char* add_user(Group *group, const char *user_name) {
    User *this_user = find_prev_user(group, user_name);
    
	int lenUser=strlen(user_name);
	int lenGroup=strlen(group->name);


	if (this_user != NULL) 
	{
		char* failMsg = myMalloc(sizeof(char)*(ADD_USER_FAIL_LEN + lenUser + lenGroup));
		snprintf(failMsg,ADD_USER_FAIL_LEN+lenUser+lenGroup,ADD_USER_FAIL,user_name,group->name);
		return failMsg;	
    }
    // ok to add a user to this group by this name
    // since users are stored by balance order and the new user has 0 balance
    // he goes first
	char* successMsg = myMalloc(sizeof(char)*(ADD_USER_SUCCESS_LEN+ lenUser +lenGroup));
	snprintf(successMsg,ADD_USER_SUCCESS_LEN+lenUser+lenGroup,ADD_USER_SUCCESS,user_name,group->name);    

	User *newuser;
    if ((newuser = myMalloc(sizeof(User))) == NULL) {
        perror("Error allocating space for new User");
        exit(1);
    }
    // set the fields of the new user node
    // first allocate space for the name
    int name_len = strlen(user_name);
    if ((newuser->name = myMalloc(name_len + 1)) == NULL) {
        perror("Error allocating space for new User name");
        exit(1);
    }
    strncpy(newuser->name, user_name, name_len + 1);
    newuser->balance = 0.0;

    // insert this user at the front of the list
    newuser->next = group->users;
    group->users = newuser;
	
	return successMsg;

}

/* Print to standard output the names and balances of all the users in group,
* one per line, and in the order that users are stored in the list, namely 
* lowest payer first.
*/

char* list_users(Group *group) {
    User *current_user = group->users;

	int maxUserName=0;

	int count=0;
	while (current_user != NULL) 
	{
		if(strlen(current_user->name) > maxUserName)
			maxUserName=strlen(current_user->name);
		count++;
	    current_user = current_user->next;
	}

	/*Allocated enough space for the balance by using the constant 
		MAX_DOUBLE_LEN. The reasoning for this is in list.h above the
			declaration of MAX_DOUBLE_LEN as well as in buxfer.c 
			before the invocation of add_xct which will be 
			responsible for setting its value.
	*/
	int maxEntryLen=(maxUserName+MAX_DOUBLE_LEN+4);
	int maxLen=(count*(maxEntryLen));
	char* users=myMalloc(sizeof(char)*maxLen);
	bzero(users,maxLen);

	current_user=group->users;
	
	int lineLen;
	char* line;
	while(current_user!=NULL)
	{
 		//create line
		lineLen=strlen(current_user->name)+MAX_DOUBLE_LEN+LIST_USER_LEN;
		line=myMalloc(sizeof(char)* lineLen);
		
		snprintf(line,lineLen,LIST_USER_MSG,current_user->name,current_user->balance);
		//concat line into big line    
		strncat(users,line,strlen(line)+2);
	
		free(line);

	    current_user = current_user->next;
    }
    return users;
}

/* Print to standard output the balance of the specified user. Return 0
* on success, or -1 if the user with the given name is not in the group.
*/
char* user_balance(Group *group, const char *user_name) {
    User * prev_user = find_prev_user(group, user_name);

	int lenUser=strlen(user_name);
	int lenGroup=strlen(group->name);

    if (prev_user == NULL) { 

		char* failMsg = myMalloc(sizeof(char)*(USER_BALANCE_FAIL_LEN+lenUser+lenGroup));
		snprintf(failMsg,USER_BALANCE_FAIL_LEN+lenUser+lenGroup,USER_BALANCE_FAIL,user_name,group->name);
        return failMsg;
    }

	char* successMsg = myMalloc(sizeof(char)*(USER_BALANCE_LEN+MAX_DOUBLE_LEN));
    if (prev_user == group->users) {
        // user could be first or second since previous is first
        if (strcmp(user_name, prev_user->name) == 0) {
            // this is the special case of first user
			snprintf(successMsg,USER_BALANCE_LEN+MAX_DOUBLE_LEN,USER_BALANCE_SUCCESS,prev_user->balance);
            return successMsg;
        }
    }
	snprintf(successMsg,USER_BALANCE_LEN+MAX_DOUBLE_LEN,USER_BALANCE_SUCCESS,prev_user->next->balance);
	return successMsg;  
}

/* Return a pointer to the user prior to the one in group with user_name. If 
* the matching user is the first in the list (i.e. there is no prior user in 
* the list), return a pointer to the matching user itself. If no matching user 
* exists, return NULL. 
*
* The reason for returning the prior user is that returning the matching user 
* itself does not allow us to change the user that occurs before the
* matching user, and some of the functions you will implement require that
* we be able to do this.
*/
User *find_prev_user(Group *group, const char *user_name) {
    User * current_user = group->users;
    // return NULL for no users in this group
    if (current_user == NULL) { 
        return NULL;
    }
    // special case where user we want is first
    if (strcmp(current_user->name, user_name) == 0) {
        return current_user;
    }
    while (current_user->next != NULL) {
        if (strcmp(current_user->next->name, user_name) == 0) {
            // we've found the user so return the previous one
            return current_user;
        }
    current_user = current_user->next;
    }
    // if we get this far without returning, current_user is last,
    // and we have already checked the last element
    return NULL;
}

/* Add the transaction represented by user_name and amount to the appropriate 
* transaction list, and update the balances of the corresponding user and group. 
* Note that updating a user's balance might require the user to be moved to a
* different position in the list to keep the list in sorted order. Returns 0 on
* success, and -1 if the specified user does not exist.
*/
char* add_xct(Group *group, const char *user_name, double amount) {
    User *this_user;
    User *prev = find_prev_user(group, user_name);

	int lenUser = strlen(user_name);
	int lenGroup = strlen(group->name);
    if (prev == NULL) 
	{
    	char* failMsg = myMalloc(sizeof(char)*(ADD_XCT_FAIL_LEN+lenUser+lenGroup));
		snprintf(failMsg,ADD_XCT_FAIL_LEN+lenUser+lenGroup,ADD_XCT_FAIL,user_name,group->name);
		return failMsg;
    }
    // but find_prev_user gets the PREVIOUS user, so correct
    if (prev == group->users) {
        // user could be first or second since previous is first
        if (strcmp(user_name, prev->name) == 0) {
            // this is the special case of first user
            this_user = prev;
        } else {
            this_user = prev->next;
        }
    } else {
        this_user = prev->next;
    }

    Xct *newxct;
    if ((newxct = malloc(sizeof(Xct))) == NULL) {
        perror("Error allocating space for new Xct");
        exit(1);
    }
    // set the fields of the new transaction node
    // first allocate space for the name
    int needed_space = strlen(user_name) + 1;
    if ((newxct->name = malloc(needed_space)) == NULL) {
         perror("Error allocating space for new xct name");
         exit(1);
    }
    strncpy(newxct->name, user_name, needed_space);
    newxct->amount = amount;

    // insert this xct  at the front of the list
    newxct->next = group->xcts;
    group->xcts = newxct;

    // first readjust the balance
    this_user->balance = this_user->balance + amount;

    // since we are only ever increasing this user's balance they can only
    // go further towards the end of the linked list
    //   So keep shifting if the following user has a smaller balance

    while (this_user->next != NULL &&
                  this_user->balance > this_user->next->balance ) {
        // he remains as this user but the user next gets shifted
        // to be behind him
        if (prev == this_user) {
            User *shift = this_user->next;
            this_user->next = shift->next;
            prev = shift;
            prev->next = this_user;
            group->users = prev;
        } else { // ordinary case in the middle
            User *shift = this_user->next;
            prev->next = shift;
            this_user->next = shift->next;
            shift->next = this_user;
            prev=shift;
        }
    }
	char* successMsg = myMalloc(sizeof(char)*(ADD_XCT_SUCCESS_LEN+lenUser+lenGroup));
	snprintf(successMsg,ADD_XCT_SUCCESS_LEN+lenUser+lenGroup,ADD_XCT_SUCCESS,user_name,group->name);
	return successMsg;
}

