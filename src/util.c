#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "util.h"

void* myMalloc(int size)
{
	void* mem = malloc(size);
	if(mem==NULL)
	{
		perror("malloc");
		exit(1);
	}
	return mem;
}

int hasKey(char* msg,char* key)
{
    if(strstr(msg,key)==NULL)
    {
        return -1;
    }
    return 0;
}

int numStrOccours(char* msg,char* key)
{
	char* str = NULL;
	int count=0;
	char* temp=msg;
	while(strstr(temp,key)!=NULL)
	{
		str=strstr(temp,key);
		temp=str+1;
		count++;
	}	
	return count;
}
int numChrOccours(char*msg,int chr)
{
	char* c = NULL;
	int count=0;
	char* temp=msg;
	while(strchr(temp,chr)!=NULL)
	{
		c=strchr(temp,chr);
		temp=c+1;
		count++;
	}
	return count;
}

