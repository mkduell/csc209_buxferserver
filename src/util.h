#ifndef UTIL_H
#define UTIL_H

void* myMalloc(int size); 
int hasKey(char* msg,char*key);
int numStrOccours(char* msg,char* key);
int numChrOccours(char* msg,int chr);
#endif
