#ifndef LISTS_H
#define LISTS_H

struct group {
	char *name;
	struct user *users;
	struct xct *xcts;
	struct group *next;
};

struct user {
	char *name;
	double balance;
	struct user *next;
};

struct xct{
	char *name;
	double amount;
	struct xct *next;
};

typedef struct group Group;
typedef struct user User;
typedef struct xct Xct;

char* add_group(Group **group_list, const char *group_name);

char* list_groups(Group *group_list);
Group *find_group(Group *group_list, const char *group_name);

char* add_user(Group *group, const char *user_name);

char* list_users(Group *group);

char* user_balance(Group *group, const char *user_name);
User *find_prev_user(Group *group, const char *user_name);

char* add_xct(Group *group, const char *user_name, double amount);

/* my constants */
/*on CDF doubles are 8 bytes, which is a max value (unsigned) of 4294967295
 10 digits total for the string interpretation.
 
 To be safe, going to only allow 9 left of the decimal
 plus optional decimal
 plus optional 2 right of the decimal
 
 = 12 total 
 
 one more for a mem fix
*/ 
#define MAX_DOUBLE_LEN 13 // 999999999.99

#define ADD_GROUP_SUCCESS "Group %s added\r\n"                                  
#define ADD_GROUP_SUCCESS_LEN 15
#define ADD_GROUP_KEY "added"
 
#define ADD_GROUP_FAIL "Group %s already exists\r\n"
#define ADD_GROUP_FAIL_LEN 24
#define ADD_GROUP_FAIL_KEY "exists"

#define ADD_USER_SUCCESS "User %s added to group %s\r\n"                        
#define ADD_USER_SUCCESS_LEN 24
#define ADD_USER_SUCCESS_KEY "added"
 
#define ADD_USER_FAIL "User %s already exists in group %s\r\n"
#define ADD_USER_FAIL_LEN 33
#define ADD_USER_FAIL_KEY "exists"

#define LIST_USER_MSG "%s: %.2f\r\n"                                            
#define LIST_USER_LEN 5

#define LIST_GROUP_MSG "%s\r\n"
#define LIST_GROUP_LEN 3

#define USER_BALANCE_SUCCESS "Balance is %.2f\r\n"                              
#define USER_BALANCE_LEN 14
#define USER_BALANCE_KEY "Balance" 
#define USER_BALANCE_FAIL "User %s not in group %s\r\n"
#define USER_BALANCE_FAIL_LEN 22
#define USER_BALANCE_FAIL_KEY "not"

#define ADD_XCT_SUCCESS "Successfully added transaction for %s in group %s\r\n"                                                                           
#define ADD_XCT_SUCCESS_LEN 48
#define ADD_XCT_SUCCESS_KEY "added" 
#define ADD_XCT_FAIL "User %s doesn't exist in %s\r\n"
#define ADD_XCT_FAIL_LEN 26
#define ADD_XCT_FAIL_KEY "exist"

#endif
