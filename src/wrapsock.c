#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <stdio.h>
#include <errno.h>


int Accept(int fd, struct sockaddr *sa, socklen_t *salenptr)
{
    int  n;

    if ( (n = accept(fd, sa, salenptr)) < 0) {
        perror("accept error");
        exit(1);
    }
    return(n);
}

void Bind(int fd, const struct sockaddr *sa, socklen_t salen)
{
    if (bind(fd, sa, salen) < 0){
        perror("bind error");
        exit(1);
    }
}

int Connect(int fd, const struct sockaddr *sa, socklen_t salen)
{
    int result;
    if ((result = connect(fd, sa, salen)) < 0) {
        perror("connect error");
    }
    return(result);
}

void Listen(int fd, int backlog)
{
    if (listen(fd, backlog) < 0) {
        perror("listen error");
        exit(1);
    }
}

int Select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds,
       struct timeval *timeout)
{
    int n;

    if ( (n = select(nfds, readfds, writefds, exceptfds, timeout)) < 0) {
        perror("select error");
        exit(1);
    }
    return(n);              /* can return 0 on timeout */
}


int Socket(int family, int type, int protocol)
{
    int n;

    if ( (n = socket(family, type, protocol)) < 0) {
        perror("socket error");
        exit(1);
    }
    return(n);
}

void Close(int fd)
{
    if (close(fd) == -1) {
        perror("close error");
        exit(1);
    }
}

ssize_t writen(int fd, const void *vptr, size_t n)
{
    size_t		nleft;
    ssize_t		nwritten;
    const char	*ptr;

    ptr = vptr;
    nleft = n;
    while (nleft > 0) {
	if ( (nwritten = write(fd, ptr, nleft)) <= 0) {
	    if (errno == EINTR)
		nwritten = 0;		/* and call write() again */
	    else
		return(-1);		/* error */
	}
	
	nleft -= nwritten;
	ptr   += nwritten;
    }
    return(n);
}


void Write(int fd, void *ptr, size_t nbytes)
{
    if (writen(fd, ptr, nbytes) != nbytes)
		perror("writen error");
}

ssize_t	 readn(int fd, void *vptr, size_t n)
{
    size_t	nleft;
    ssize_t	nread;
    char	*ptr;
    
    ptr = vptr;
    nleft = n;
    while (nleft > 0) {
	if ( (nread = read(fd, ptr, nleft)) < 0) {
	    if (errno == EINTR)
		nread = 0;		/* and call read() again */
	    else
		return(-1);
	} else if (nread == 0)
	    break;				/* EOF */
	
	nleft -= nread;
	ptr   += nread;
    }
    return(n - nleft);		/* return >= 0 */
}

ssize_t Read(int fd, void *ptr, size_t nbytes)
{
    ssize_t n;
    
    if ( (n = readn(fd, ptr, nbytes)) < 0)
	perror("readn error");
    return(n);
}
