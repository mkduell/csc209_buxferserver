#ifndef BUXFER_H
#define BUXFER_H

#include "lists.h"

#define MAXLINE 256
#define MAXCLIENTS 30

typedef struct {
		int soc;
		char buf[MAXLINE];
		char* name;
		int curpos;
} Client;

void releaseClient(Client* c);
int runBuxferCommand(char* input,Client* c,Group** group_list,Client* client_list);

#endif
